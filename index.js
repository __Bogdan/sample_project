var express = require('express');

var app = express();

app.get('/', function(req, res){
  res.send('Hello World');
});

var server = app.listen(3000);
console.log('Express started on port 3000');

module.exports = server;

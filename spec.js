var should = require('should');
var request = require('supertest');
var expect = require('expect');

describe('Server',  function() {

  var server;

  beforeEach( function() {
    server = require('./index');
  });

  afterEach( function() {
    server.close();
  });

  it('Should respond with "Hello World"', function(done) {
    request(server)
      .get('/')
      .expect(200)
      .then(response => {
        expect(response.text).toBe('Hello World');
        done();
      })
  });

  it('Should fail', function(done){
    request(server)
      .get('/foo/bar')
      .expect(404, done);
  })

});
